package com.epam.gomel.tat.lesson_5.pages;

import org.openqa.selenium.By;

/**
 * Created by Konstantsin_Simanenk on 3/17/2015.
 */
public class MailSentListPage extends AbstractBasePage {
    private static final String MAIL_LINK_LOCATOR_PATTERN = "//label[text()='Отправленные']/ancestor::div[@class='block-messages']//a[.//*[text()='%s']]";

    public boolean isMessagePresent(String subject) {
        browser.waitForVisible(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, subject)));
        return true;
    }

}
