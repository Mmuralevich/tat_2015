package com.epam.gomel.tat.lesson_4.UI;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by maryna_muralevich on 3/17/2015.
 */
public class Browser {

    public  final int WAIT_ELEMENT_TIMEOUT = 20;
    private WebDriver driver;

    private Browser(WebDriver driver){
        this.driver = driver;
    }

    public static Browser init(){
        WebDriver driver = new FirefoxDriver();
        return new Browser(driver);
    }



    public void open(String url) {
        driver.get(url);
    }

    public void kill() {
        driver.quit();
    }

    public void click(By locator) {
        driver.findElement(locator).click();
    }

    public void type(By locator) {
        driver.findElement(locator).sendKeys();
    }

    public void attachFile(By locator) {
        driver.findElement(locator).sendKeys();
    }





}
