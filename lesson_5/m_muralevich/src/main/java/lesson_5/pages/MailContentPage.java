package lesson_5.pages;

import org.openqa.selenium.By;
import org.testng.Assert;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Alexander on 19.03.2015.
 */
public class MailContentPage extends AbstractBasePage {
    public static final By DOWNLOAD_ATTACHMENT_LINK_LOCATOR = By.xpath("//div[@class='block-messages-list-box b-layout__first-pane']/div[2]//div[@class='b-messages']/div[1]//span[@class='b-messages__subject']");

    public boolean isAttachmentCorrect (String pathToAttachment) {
        browser.click(DOWNLOAD_ATTACHMENT_LINK_LOCATOR);
        try {
            FileReader uploadedFile = new FileReader(pathToAttachment);
            FileReader downloadedFile = new FileReader("\\"+pathToAttachment);
            BufferedReader txtReader = new BufferedReader(uploadedFile);
            BufferedReader txtReader2 = new BufferedReader(downloadedFile);
            try {
                Assert.assertTrue(txtReader.readLine().equals(txtReader2.readLine()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return true;
    }


}
