package lesson_5.pages;

import org.openqa.selenium.By;

/**
 * Created by Alexander on 19.03.2015.
 */
public class MailDeleteListPage extends AbstractBasePage {

    private static final By SUBJECT_DELETED_MAIL_LOCATOR = By.xpath("//div[@class='block-messages-list-box b-layout__first-pane']/div[2]//div[@class='b-messages']/div[1]//span[@class='b-messages__subject']");

    public boolean isMessagePresent(String subject) {
        browser.waitForVisible(SUBJECT_DELETED_MAIL_LOCATOR);
        return true;
    }
}
