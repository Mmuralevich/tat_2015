package lesson_5.pages;

import lesson_5.ui.Browser;

/**
 * Created by Konstantsin_Simanenk on 3/17/2015.
 */
public abstract class AbstractBasePage {

    protected Browser browser;

    public AbstractBasePage() {
        this.browser = Browser.get();
    }

}
