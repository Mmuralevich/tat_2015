package lesson_5.pages;

import org.openqa.selenium.By;

/**
 * Created by Konstantsin_Simanenk on 3/17/2015.
 */
public class YandexFirstPage extends AbstractBasePage {
    public static final String BASE_URL = "http://yandex.ru";

    private static final By LOGIN_INPUT_LOCATOR = By.name("login");
    private static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");

    public YandexFirstPage open() {
        browser.open(BASE_URL);
        return this;
    }

    public MailboxBasePage login(String login, String password) {
        browser.type(LOGIN_INPUT_LOCATOR, login);
        browser.type(PASSWORD_INPUT_LOCATOR, password);
        browser.submit(PASSWORD_INPUT_LOCATOR);
        return new MailboxBasePage();
    }



}
