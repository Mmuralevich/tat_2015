package lesson_5.pages;

import org.openqa.selenium.By;

/**
 * Created by Konstantsin_Simanenk on 3/17/2015.
 */
public class ComposeMailPage extends AbstractBasePage {
    private static final String PROJECT_PATH = "user.dir";
    private static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    private static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    private static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    private static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    private static final By ATTACHE_FILE_INPUT_LOCATOR = By.xpath("//input[@name='att']");

    public MailboxBasePage sendMail(String mailTo, String mailSubject, String mailContent) {
        browser.type(TO_INPUT_LOCATOR, mailTo);
        browser.type(SUBJECT_INPUT_LOCATOR, mailSubject);
        browser.type(MAIL_TEXT_LOCATOR, mailContent);

        browser.click(SEND_MAIL_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
        // wait for message
        return new MailboxBasePage();

    }

    public MailboxBasePage sendMailWithAttachment(String mailTo, String mailSubject, String mailContent, String pathToAttach) {
        browser.type(TO_INPUT_LOCATOR, mailTo);
        browser.type(SUBJECT_INPUT_LOCATOR, mailSubject);
        browser.type(MAIL_TEXT_LOCATOR, mailContent);
        String projectPath = System.getProperty(PROJECT_PATH);
        browser.attachFile(ATTACHE_FILE_INPUT_LOCATOR, projectPath+pathToAttach);
        browser.click(SEND_MAIL_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
        // wait for message
        return new MailboxBasePage();
    }
}
