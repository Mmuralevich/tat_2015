package lesson_5.pages;

import org.openqa.selenium.By;

/**
 * Created by Konstantsin_Simanenk on 3/17/2015.
 */
public class MailInboxListPage extends AbstractBasePage {

    private static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    private static final By MAIL_CHECKBOX_LOCATOR = By.xpath("//div[@class='block-messages-list-box b-layout__first-pane']/div[1]//div[1]/label[@class='b-messages__message__checkbox']/input");
    public static final By DELETE_BUTTON_LOCATOR = By.xpath("//a[@data-action='delete']");

    public ComposeMailPage openComposeMailPage() {
        browser.click(COMPOSE_BUTTON_LOCATOR);
        return new ComposeMailPage();
    }

    public MailboxBasePage deleteEmail() {
        browser.click(MAIL_CHECKBOX_LOCATOR);
        browser.click(DELETE_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailboxBasePage();
    }
}
