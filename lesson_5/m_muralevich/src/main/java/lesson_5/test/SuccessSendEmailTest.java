package lesson_5.test;

import lesson_5.pages.MailboxBasePage;
import lesson_5.pages.YandexFirstPage;
import lesson_5.ui.Browser;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

/**
 * Created by Alexander on 19.03.2015.
 */
public class SuccessSendEmailTest {

    private String userLogin = "mashafakelogin";
    private String userPassword = "mashafake";
    private String mailTo = "mashafakelogin@yandex.ru";
    private String mailSubject = "test subject" + Math.random() * 100000000;
    private String mailContent = "mail content" + Math.random() * 100000000;

    @Test(description = "Success send email without attachment")
    public void successSendEmail() {
        MailboxBasePage mailbox = new YandexFirstPage().open()
                .login(userLogin, userPassword)
                .openInboxPage()
                .openComposeMailPage()
                .sendMail(mailTo, mailSubject, mailContent);

        Assert.assertTrue(mailbox.openSentPage().isMessagePresent(mailSubject), "Mail has not been sent");
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        Browser.get().kill();
    }
}
