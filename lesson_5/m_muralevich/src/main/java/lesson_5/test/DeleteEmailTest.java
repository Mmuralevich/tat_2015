package lesson_5.test;

import com.google.common.base.Predicate;
import lesson_5.pages.MailDeleteListPage;
import lesson_5.pages.YandexFirstPage;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by Alexander on 19.03.2015.
 */
public class DeleteEmailTest {

    // Test data
    private String userLogin = "mashafakelogin";
    private String userPassword = "mashafake";
    private String mailTo = "mashafakelogin@yandex.ru";
    private String mailSubject = "test subject" + Math.random() * 100000000;
    private String mailContent = "mail content" + Math.random() * 100000000;


    @Test(description = "Delete email")
    public void deleteEmail() {
        MailDeleteListPage mailDelete = new YandexFirstPage().open()
                .login(userLogin, userPassword)
                .openInboxPage()
                .openComposeMailPage()
                .sendMail(mailTo,mailSubject, mailContent)
                .openInboxPage()
                .deleteEmail()
                .openDeletePage();

        Assert.assertTrue(mailDelete.isMessagePresent(mailSubject), "Message was not deleted");

    }

}
