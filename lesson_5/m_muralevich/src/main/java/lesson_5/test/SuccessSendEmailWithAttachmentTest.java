package lesson_5.test;

import lesson_5.pages.MailContentPage;
import lesson_5.pages.MailboxBasePage;
import lesson_5.pages.YandexFirstPage;
import lesson_5.ui.Browser;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

/**
 * Created by Alexander on 19.03.2015.
 */
public class SuccessSendEmailWithAttachmentTest {

    private String userLogin = "mashafakelogin";
    private String userPassword = "mashafake";
    private String mailTo = "mashafakelogin@yandex.ru";
    private String mailSubject = "test subject" + Math.random() * 100000000;
    private String mailContent = "mail content" + Math.random() * 100000000;
    private String pathToAttachment = "\\attachment.txt";

    @Test(description = "Success send email with attachment")
    public void successSendEmailWithAttachmentTest() {
        MailContentPage mailbox = new YandexFirstPage().open()
                .login(userLogin, userPassword)
                .openInboxPage()
                .openComposeMailPage()
                .sendMailWithAttachment(mailTo, mailSubject, mailContent, pathToAttachment)
                .openSentPage()
                .openMailContentPage();


        Assert.assertTrue(mailbox.isAttachmentCorrect(pathToAttachment), "Mail with attachment has not been sent");
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        Browser.get().kill();
    }
}
