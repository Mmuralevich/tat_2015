package com.epam.gomel.tat.lesson_4;

import com.google.common.base.Predicate;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by Alexander on 15.03.2015.
 */
public class MarkEmailAsSpamTest {
    // AUT data
    public static final String BASE_URL = "http://www.yandex.ru";

    // UI data
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By SUBJECT_MAIL_LOCATOR = By.xpath("//div[@class='b-messages']/div[1]//span[@class='b-messages__subject']");
    public static final By MAIL_CHECKBOX_LOCATOR = By.xpath("//div[@class='b-messages']/div[1]/label[@class='b-messages__message__checkbox']/input[1]");
    public static final By SPAM_BUTTON_LOCATOR = By.xpath("//a[@data-action='tospam']");
    public static final By SPAM_LINK_LOCATOR = By.xpath("//a[@href='#spam']");
    public static final By SUBJECT_SPAM_MAIL_LOCATOR = By.xpath("//div[@class='block-messages-list-box b-layout__first-pane']/div[2]//div[@class='b-messages']/div[1]//span[@class='b-messages__subject'][1]");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox'][@class='b-folders__folder__link']");

    // Tools data
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 5;
    private WebDriver driver;

    // Test data
    private String userLogin = "mashafakelogin";
    private String userPassword = "mashafake";
    private String mailTo = "mashafakelogin@yandex.ru";
    private String mailSubject = "test subject" + Math.random() * 100000000;
    private String mailContent = "mail content" + Math.random() * 100000000;

    @BeforeClass(description = "Prepare browser")
    public void prepareBrowser() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    @Test(description = "Success send email")
    public void successSendEmail() {
        driver.get(BASE_URL);
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();

        WebElement inboxLink = driver.findElement(INBOX_LINK_LOCATOR);
        inboxLink.click();
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(mailTo);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(mailSubject);
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(mailContent);

        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
        waitForAjaxProcessed();

    }

    @Test(description = "Mark email as a spam", dependsOnMethods = "successSendEmail")
    public void markEmailAsSpam() {
        WebElement inboxLink = driver.findElement(INBOX_LINK_LOCATOR);
        inboxLink.click();
        String subjectMail = driver.findElement(SUBJECT_MAIL_LOCATOR).getText();
        WebElement mailCheckbox = driver.findElement(MAIL_CHECKBOX_LOCATOR);
        mailCheckbox.click();

        WebElement spamButton = driver.findElement(SPAM_BUTTON_LOCATOR);
        spamButton.click();

        waitForAjaxProcessed();

        WebElement spamLink = driver.findElement(SPAM_LINK_LOCATOR);
        spamLink.click();
        driver.findElement(SUBJECT_SPAM_MAIL_LOCATOR).equals(subjectMail);

    }

    public void waitForAjaxProcessed() {
        new WebDriverWait(driver, 20).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
    }

    @AfterClass(description = "Close browser")
       public void clearBrowser() {
            driver.quit();
    }
}
