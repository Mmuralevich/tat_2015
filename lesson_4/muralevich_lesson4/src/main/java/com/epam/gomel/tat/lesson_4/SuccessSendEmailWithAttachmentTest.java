package com.epam.gomel.tat.lesson_4;

import com.google.common.base.Predicate;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by Alexander on 15.03.2015.
 */
public class SuccessSendEmailWithAttachmentTest {
    // AUT data
    public static final String BASE_URL = "http://www.yandex.ru";

    // UI data
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By OUTBOX_LINK_LOCATOR = By.xpath("//a[@href='#sent']");
    public static final By SUBJECT_SENDED_MAIL_LOCATOR = By.xpath("//div[@class='block-messages-list-box b-layout__first-pane']/div[1]//div[@class='b-messages']/div[1]//span[@class='b-messages__subject']");
    public static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox'][@class='b-folders__folder__link']");
    public static final By ATTACH_FILES_BUTTON_LOCATOR = By.xpath("//input[@name='att']");
    public static final By DOWNLOAD_ATTACHMENT_LINK_LOCATOR = By.xpath("//div[@class='b-message-attachments_head']//span[@class='b-file__actions']/a[2]");
    public static final String PATH_TO_ATTACHMENT = "\\attachment.txt";


    // Tools data
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 5;
    private WebDriver driver;

    // Test data
    private String userLogin = "mashafakelogin";
    private String userPassword = "mashafake";
    private String mailTo = "mashafakelogin@yandex.ru";
    private String mailSubject = "test subject" + Math.random() * 100000000;
    private String mailContent = "mail content" + Math.random() * 100000000;

    @BeforeClass(description = "Prepare browser")
    public void prepareBrowser() {
        FirefoxProfile fxProfile = new FirefoxProfile();

        fxProfile.setPreference("browser.download.folderList",2);
        fxProfile.setPreference("browser.download.manager.showWhenStarting",false);
        fxProfile.setPreference("browser.download.dir","user.dir");
        fxProfile.setPreference("browser.helperApps.neverAsk.saveToDisk","text/plain");

        driver = new FirefoxDriver(fxProfile);
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    @Test(description = "Success send email with attachment")
    public void successSendEmailWithAttachment() {
        driver.get(BASE_URL);
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();

        WebElement inboxLink = driver.findElement(INBOX_LINK_LOCATOR);
        inboxLink.click();
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(mailTo);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(mailSubject);
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(mailContent);
        String projectPath = System.getProperty("user.dir");
        WebElement attachFilesButton = driver.findElement(ATTACH_FILES_BUTTON_LOCATOR);

        attachFilesButton.sendKeys(projectPath+PATH_TO_ATTACHMENT);
        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
        waitForAjaxProcessed();

        WebElement outboxLink = driver.findElement(OUTBOX_LINK_LOCATOR);
        outboxLink.click();
        WebElement subjectSendedMail = driver.findElement(SUBJECT_SENDED_MAIL_LOCATOR);
        subjectSendedMail.click();

        WebElement downloadAttachmentLink = driver.findElement(DOWNLOAD_ATTACHMENT_LINK_LOCATOR);
        downloadAttachmentLink.click();



        try {
            FileReader uploadedFile = new FileReader(PATH_TO_ATTACHMENT);
            FileReader downloadedFile = new FileReader("\\"+PATH_TO_ATTACHMENT);
            BufferedReader txtReader = new BufferedReader(uploadedFile);
            BufferedReader txtReader2 = new BufferedReader(downloadedFile);
            try {
              Assert.assertTrue(txtReader.readLine().equals(txtReader2.readLine()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void waitForAjaxProcessed() {
        new WebDriverWait(driver, 20).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
    }

    //@AfterClass(description = "Close browser")
    //    public void clearBrowser() {
    //    driver.quit();
    //}


}
