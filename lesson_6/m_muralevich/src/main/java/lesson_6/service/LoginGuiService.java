package lesson_6.service;

import lesson_6.bo.common.WrongAccount;
import lesson_6.pages.YandexFirstPage;
import lesson_6.bo.common.Account;
import lesson_6.exception.TestCommonRuntimeException;
import lesson_6.pages.MailboxBasePage;
import lesson_6.reporting.Logger;


public class LoginGuiService {

    public void loginToAccountMailbox(Account account) {
        Logger.log.info("Login to account " + account.getLogin());
        MailboxBasePage mailbox = new YandexFirstPage().open().login(account.getLogin(), account.getPassword());
        String userEmail = mailbox.getUserEmail();
        if (userEmail == null && !userEmail.equals(account.getEmail())) {
            throw new TestCommonRuntimeException("Login failed. User Mail : '" + userEmail + "'");
        }
    }

    public void checkLoginImpossible(WrongAccount wrongAccount) {
        Logger.log.info("Login to account " + wrongAccount.getLogin());
        MailboxBasePage mailbox = new YandexFirstPage().open().login(wrongAccount.getLogin(), wrongAccount.getWrongPassword());
        String userEmail = mailbox.getErrorMessage();
        if (userEmail == null && !userEmail.equals(wrongAccount.getErrorMessage())) {
            throw new TestCommonRuntimeException("Login failed. User Mail : '" + userEmail + "'");
        }
    }
}
