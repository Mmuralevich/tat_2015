package lesson_6.service;

import lesson_6.bo.mail.MailLetter;
import lesson_6.exception.TestCommonRuntimeException;
import lesson_6.pages.*;
import lesson_6.reporting.Logger;

public class MailGuiService {

    public void sendMail(MailLetter letter) {
        Logger.log.info("Send mail to " + letter.getReceiver());
        MailInboxListPage mailbox = new MailboxBasePage().openInboxPage();
        mailbox.openComposeMailPage().sendMail(letter.getReceiver(), letter.getSubject(), letter.getContent(), letter.getAttach());

    }

    public void sendMailWithAttachmnet(MailLetter letter) {
        Logger.log.info("Send mail with attachmnet to " + letter.getReceiver());
        MailInboxListPage mailbox = new MailboxBasePage().openInboxPage();
        mailbox.openComposeMailPage().sendMailWithAttach(letter.getReceiver(), letter.getSubject(), letter.getContent(), letter.getAttach());
        MailContentPage mailbox1 = new MailboxBasePage().openSentPage().openMailContentPage().downloadAttachment(letter.getAttach());
        if ( !mailbox1.isAttachmentCorrect(letter.getAttach())){
            throw new TestCommonRuntimeException("Attachment is not correct in the mail  : '" + letter.getSubject() + "'");
        };
    }

    public void checkMailInSentList(MailLetter letter) {
        Logger.log.info("Check that mail with subject " + letter.getReceiver()+" exists in sent list");
        MailSentListPage mailbox = new MailboxBasePage().openSentPage();
        String messageSubject = mailbox.getMessageSubject(letter.getSubject());
        if (messageSubject == null && !(messageSubject.equals(letter.getSubject()))) {
            throw new TestCommonRuntimeException("Mail has not been sent with mail subject : '" + messageSubject + "'");
        }
    }

    public void markMailAsDelete(MailLetter letter) {
        Logger.log.info("Check that mail with subject " + letter.getReceiver()+" was deleted");
        MailDeleteListPage mailbox = new MailboxBasePage().openInboxPage().deleteEmail().openDeletePage();
        String messageSubject = mailbox.getMessageSubject();
        if (messageSubject == null && !(messageSubject.equals(letter.getSubject()))) {
            throw new TestCommonRuntimeException("Mail has not been deleted with mail subject : '" + messageSubject + "'");
        }
    }


    public void checkMailAsSpam(MailLetter letter) {
        Logger.log.info("Check that mail with subject " + letter.getReceiver()+" was marked as Spam");
        MailSpamListPage mailbox = new MailboxBasePage().openInboxPage().markEmailAsSpam().openSpamPage();
        String messageSubject = mailbox.getMessageSubject();
        if (messageSubject == null && !(messageSubject.equals(letter.getSubject()))) {
            throw new TestCommonRuntimeException("Mail has not been marked as spam with mail subject : '" + messageSubject + "'");
        }
    }

    public void checkMailAsNotSpam(MailLetter letter) {
        Logger.log.info("Check that mail with subject " + letter.getReceiver()+" was marked as not a Spam");
        MailInboxListPage mailbox = new MailboxBasePage().openInboxPage().markEmailAsSpam().openSpamPage().markEmailAsNotSpam().openInboxPage();
        String messageSubject = mailbox.getMessageSubject();
        if (messageSubject == null && !(messageSubject.equals(letter.getSubject()))) {
            throw new TestCommonRuntimeException("Mail has not been marked as spam with mail subject : '" + messageSubject + "'");
        }
    }

}
