package lesson_6.ui;

import lesson_6.exception.TestCommonRuntimeException;

/**
 * Created by Maryna_Muralevich on 3/27/2015.
 */
public enum BrowserType {
    FIREFOX("firefox"),
    CHROME("chrome");

    private String alias;

    BrowserType(String alias) {
        this.alias = alias;
    }

    public static BrowserType getTypeByAlias(String alias) {
        for(BrowserType type: BrowserType.values()){
            if(type.getAlias().equals(alias.toLowerCase())){
                return type;
            }
        }
        throw new TestCommonRuntimeException("No such enum value");
    }

    public String getAlias() {
        return alias;
    }
}

