package lesson_6.utils;

import lesson_6.exception.TestCommonRuntimeException;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Maryna_Muralevich on 3/27/2015.
 */
public class FileTools {
    private static final Logger LOGGER = Logger.getLogger(FileTools.class);

    private static final String FILE_TYPE = "FILE";

    public static String getFileType(File file) {
        return getFileType(file.getName());
    }

    public static String getFileType(String filename) {
        String[] parts = filename.split("\\.");
        if (parts.length > 1) {
            return parts[parts.length - 1].toUpperCase();
        } else {
            return FILE_TYPE;
        }
    }

    public static List<String> getFilesPath(String[] filesPath) {
        try {
            lesson_6.reporting.Logger.log.info("Start work with File with local path : " + filesPath);
            List<String> suites = new ArrayList<String>();

            for (String suite : filesPath) {

                lesson_6.reporting.Logger.info("Run suit:" + suite);

                suites.add(suite);

            }

            return suites;
        } catch (TestCommonRuntimeException e) {
            LOGGER.error(e.getMessage(), e);
        }

        return null;
    }

    public static String getCanonicalPathToResourceFile(String resourceFileLocalPath) {
        try {
            URL url = FileTools.class.getResource(resourceFileLocalPath);
            File file = new File(url.getPath());
            return file.getCanonicalPath();
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }

        return null;
    }

}
