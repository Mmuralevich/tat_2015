package lesson_6.utils;

import lesson_6.cli.TestRunnerOptions;
import lesson_6.ui.BrowserType;

import java.util.List;

/**
 * Created by Maryna_Muralevich on 3/27/2015.
 */
public class GlobalConfig {

    private static BrowserType browserType = BrowserType.FIREFOX;

    private static ParallelMode parallelMode = ParallelMode.FALSE;

    private static List<String> files;

    private static int threadCount = 1;

    public static void updateFromOptions(TestRunnerOptions options) {
        browserType = BrowserType.getTypeByAlias(options.browserType);
        parallelMode = ParallelMode.getTypeByAlias(options.parallelMode);
        threadCount = options.threadCount;
        files = FileTools.getFilesPath(options.suites);
    }

    public static BrowserType getBrowserType() {
        return browserType;
    }

    public static void setBrowserType(BrowserType browserType) {
        GlobalConfig.browserType = browserType;
    }

    public static ParallelMode getParallelMode() {
        return parallelMode;
    }

    public static void setParallelMode(ParallelMode parallelMode) {
        GlobalConfig.parallelMode = parallelMode;
    }

    public static int getThreadCount() {
        return threadCount;
    }

    public static void setThreadCount(int threadCount) {
        GlobalConfig.threadCount = threadCount;
    }

    public static List<String> getFiles() {
        return files;
    }

    public static void setFiles(List<String> files) {
        GlobalConfig.files = files;
    }
}
