package lesson_6.cli;

import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.StringArrayOptionHandler;

/**
 * Created by Maryna_Muralevich on 3/27/2015.
 */
public class TestRunnerOptions {
    @Option(name = "-bt", usage = "browser type", required = true)
    public String browserType;

    @Option(name = "-tc", usage = "thread count")
    public int threadCount;

    @Option(name = "-mode", usage = "parallel mode: false, tests, classes")
    public String parallelMode;

    @Option(name = "-suites", handler = StringArrayOptionHandler.class)
    public String[] suites;
}
