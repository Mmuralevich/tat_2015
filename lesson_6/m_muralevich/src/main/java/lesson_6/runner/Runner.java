package lesson_6.runner;

import lesson_6.cli.TestRunnerOptions;
import lesson_6.reporting.CustomTestNgListener;
import lesson_6.reporting.Logger;
import lesson_6.utils.FileTools;
import lesson_6.utils.GlobalConfig;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import java.util.ArrayList;
import java.util.List;

public class Runner {

    private Runner(String[] args) {
        parseCli(args);
    }

    public static void main(String[] args) {
        new Runner(args).runTests();



    }

    private void parseCli(String[] args) {
        TestRunnerOptions options = new TestRunnerOptions();
        CmdLineParser parser = new CmdLineParser(options);
        try {
            parser.parseArgument(args);
            GlobalConfig.updateFromOptions(options);
            System.out.println();
        } catch( CmdLineException e ) {
            Logger.error("Error exists while parse arguments", e);
            parser.printUsage(System.err);
            System.err.println();
            return;
        }
        System.out.println(options);
    }
    private void runTests() {
        TestNG testNG = new TestNG();
        testNG.addListener(new CustomTestNgListener());

        testNG.setTestSuites(GlobalConfig.getFiles());

        testNG.setParallel(GlobalConfig.getParallelMode().getAlias());
        testNG.setThreadCount(GlobalConfig.getThreadCount());
        testNG.run();
    }

}
