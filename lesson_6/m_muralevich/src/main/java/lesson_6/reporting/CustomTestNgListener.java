package lesson_6.reporting;

import lesson_6.ui.Browser;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.internal.IResultListener2;

public class CustomTestNgListener implements IResultListener2 {

    @Override
    public void onStart(ITestContext context) {
        Logger.log.info("START : " + context.getCurrentXmlTest().getName());
    }

    @Override
    public void onFinish(ITestContext context) {
        Browser.kill();
        Logger.log.info("FINISH : " + context.getName());
    }

    @Override
    public void onTestStart(ITestResult result) {
        Logger.log.info("TEST METHOD START : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
        Logger.log.error("test start ", result.getThrowable());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        Logger.log.info("TEST METHOD SUCCESS : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
        Logger.log.error("test success ", result.getThrowable());
    }

    @Override
    public void onTestFailure(ITestResult result) {
        Logger.log.info("TEST METHOD FAILED : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
        Logger.log.error("Test failed", result.getThrowable());
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        Logger.log.info("TEST METHOD SKIPPED : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
        Logger.log.error("test method skipped ", result.getThrowable());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        Logger.log.info("TEST METHOD FAILED : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
        Logger.log.error("test method failed ", result.getThrowable());
    }

    @Override
    public void beforeConfiguration(ITestResult result) {
        Logger.log.info("CONFIG START : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
        Logger.log.error("Config start", result.getThrowable());
    }

    @Override
    public void onConfigurationSuccess(ITestResult result) {
        Logger.log.info("CONFIG SUCCESS : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
        Logger.log.error("Config success", result.getThrowable());
    }

    @Override
    public void onConfigurationFailure(ITestResult result) {
        Logger.log.info("CONFIG FAILED : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
        Logger.log.error("Config failed", result.getThrowable());
    }

    @Override
    public void onConfigurationSkip(ITestResult result) {
        Logger.log.info("CONFIG SKIPPED : " + result.getMethod().getInstance().getClass().getCanonicalName() + "." + result.getMethod().getMethodName());
        Logger.log.error("Config skipped", result.getThrowable());
    }
}
