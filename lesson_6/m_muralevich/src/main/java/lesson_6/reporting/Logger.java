package lesson_6.reporting;

import org.apache.xpath.operations.String;

public class Logger {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Logger.class);

    public static void error(String s) {
        log.error(s);
    }

    public static void error(Object s, Throwable e) {
        log.error(s, e);
    }


    public static void info(Object s) {
        log.error(s);
    }

    public static void info(String s, Throwable e) {
        log.error(s, e);
    }

    public static void warn(String s) {
        log.error(s);
    }

    public static void warn(String s, Throwable e) {
        log.error(s, e);
    }

    public static void trace(String s) {
        log.error(s);
    }

    public static void trace(String s, Throwable e) {
        log.error(s, e);
    }

    public static void debug(String s) {
        log.error(s);
    }

    public static void debug(String s, Throwable e) {
        log.error(s, e);
    }

    public static void fatal(String s) {
        log.error(s);
    }

    public static void fatal(String s, Throwable e) {
        log.error(s, e);
    }
}
