package lesson_6.pages;

import lesson_6.ui.Browser;


public abstract class AbstractBasePage {

    protected Browser browser;

    public AbstractBasePage() {
        this.browser = Browser.get();
    }

}
