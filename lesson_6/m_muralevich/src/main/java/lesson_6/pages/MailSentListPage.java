package lesson_6.pages;

import org.openqa.selenium.By;

/**
 * Created by Konstantsin_Simanenk on 3/17/2015.
 */
public class MailSentListPage extends AbstractBasePage {
    private static final String MAIL_LINK_LOCATOR_PATTERN = "//div[@class='b-messages']/div[1]//span[@class='b-messages__subject'][text()='%s']";
    private static final By SUBJECT_SENDED_MAIL_LOCATOR = By.xpath("//div[@class='block-messages-list-box b-layout__first-pane']/div[2]//div[@class='b-messages']/div[1]//span[@class='b-messages__subject']");

    public boolean isMessagePresent(String subject) {
        browser.waitForVisible(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, subject)));
        return true;
    }

    public String getMessageSubject(String subject) {
        return browser.getTextToCompare(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, subject)));
    }

    public MailContentPage openMailContentPage() {
        browser.click(SUBJECT_SENDED_MAIL_LOCATOR);
        return new MailContentPage();
    }

}
