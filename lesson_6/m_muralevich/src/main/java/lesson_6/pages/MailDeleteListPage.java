package lesson_6.pages;

import org.openqa.selenium.By;

/**
 * Created by Alexander on 19.03.2015.
 */
public class MailDeleteListPage extends AbstractBasePage {

    private static final By SUBJECT_DELETED_MAIL_LOCATOR = By.xpath("//div[@class='block-messages-list-box b-layout__first-pane']/div[2]//div[@class='b-messages']/div[1]//span[@class='b-messages__subject']");

    public String getMessageSubject() {
        return browser.getTextToCompare(SUBJECT_DELETED_MAIL_LOCATOR);

    }
}
