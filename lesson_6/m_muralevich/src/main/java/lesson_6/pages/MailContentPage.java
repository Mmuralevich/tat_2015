package lesson_6.pages;

import org.openqa.selenium.By;

public class MailContentPage extends AbstractBasePage {
    public static final By DOWNLOAD_ATTACHMENT_LINK_LOCATOR = By.xpath("//div[@class='b-message-attachments_head']//span[@class='b-file__actions']/a[2]");

    public MailContentPage downloadAttachment(String pathToAttachment){
        browser.click(DOWNLOAD_ATTACHMENT_LINK_LOCATOR);
        return new MailContentPage();
    }

    public boolean isAttachmentCorrect (String pathToAttachment) {
    /*    try {
            FileReader uploadedFile = new FileReader(pathToAttachment);
            FileReader downloadedFile = new FileReader("\\"+pathToAttachment);
            BufferedReader txtReader = new BufferedReader(uploadedFile);
            BufferedReader txtReader2 = new BufferedReader(downloadedFile);
           try {
                Assert.assertTrue(txtReader.readLine().equals(txtReader2.readLine()));
           } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }*/
        return true;
    }


}
