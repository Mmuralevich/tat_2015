package lesson_6.bo.mail;

import static lesson_6.utils.RandomUtils.getRandomString;

public class MailBuilder {
    private static String RECEIVER = "mashafakelogin@yandex.ru";
    private static String SUBJECT = "test subject";
    private static String CONTENT = "mail content";
    private static String ATTACH = "\\attachment.txt";

    public static MailLetter getDefaultMailLetter() {
        return new MailLetter(RECEIVER, getRandomString(SUBJECT, 100000000), getRandomString(CONTENT, 100000000));
    }
    public static MailLetter getMailLetterWithAttachment() {
        return new MailLetter(RECEIVER, getRandomString(SUBJECT, 100000000), getRandomString(CONTENT, 100000000), ATTACH);
    }
}
