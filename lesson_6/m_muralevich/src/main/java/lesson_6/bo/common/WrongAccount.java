package lesson_6.bo.common;

public class WrongAccount {
    private String login;
    private String wrongPassword;
    private String errorMessage;

    public WrongAccount(String login, String wrongPassword, String errorMessage) {
        this.login = login;
        this.wrongPassword = wrongPassword;
        this.errorMessage = errorMessage;
    }



    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getWrongPassword() {
        return wrongPassword;
    }

    public void setWrongPassword(String password) {
        this.wrongPassword = wrongPassword;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
