package lesson_6.bo.common;

public class AccountBuilder {
    private static String USER_LOGIN = "mashafakelogin";
    private static String USER_PASSWORD = "mashafake";
    private static String USER_WRONG_PASSWORD = "mashafake1";
    private static String MAIL_ACCOUNT_LINK = "mashafakelogin@yandex.ru";
    private static String ERROR_TEXT = "Неправильная пара логин-пароль! Авторизоваться не удалось.";

    public static Account getDefaultAccount() {
        return new Account(USER_LOGIN, USER_PASSWORD, MAIL_ACCOUNT_LINK);
    }

    public static WrongAccount getWrongAccount() {
        return new WrongAccount(USER_LOGIN, USER_WRONG_PASSWORD, ERROR_TEXT);
    }
}
