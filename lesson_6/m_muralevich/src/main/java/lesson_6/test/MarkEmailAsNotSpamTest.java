package lesson_6.test;

import lesson_6.bo.common.Account;
import lesson_6.bo.common.AccountBuilder;
import lesson_6.bo.mail.MailBuilder;
import lesson_6.bo.mail.MailLetter;
import lesson_6.service.LoginGuiService;
import lesson_6.service.MailGuiService;
import lesson_6.ui.Browser;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class MarkEmailAsNotSpamTest {

    private LoginGuiService loginGuiService = new LoginGuiService();
    private MailGuiService mailGuiService = new MailGuiService();
    private MailLetter letter = MailBuilder.getDefaultMailLetter();
    private Account defaultAccount= AccountBuilder.getDefaultAccount();

    @BeforeClass(description = "Login to account mailbox")
    public void loginToAccount() {
        loginGuiService.loginToAccountMailbox(defaultAccount);
    }

    @Test(description = "Send mail")
    public void sendMail() {
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Mark email as spam", dependsOnMethods = "sendMail")
    public void markEmailAsSpamTest() {
        mailGuiService.checkMailAsSpam(letter);
    }

    @Test(description = "Mark email as not a spam", dependsOnMethods = "markEmailAsSpamTest")
    public void markEmailAsNotSpamTest() {
        mailGuiService.checkMailAsNotSpam(letter);
    }

    @AfterClass
    public void stopBrowser() {
        Browser.kill();
    }
}
