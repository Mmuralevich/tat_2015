package lesson_6.test;

import lesson_6.bo.common.Account;
import lesson_6.bo.common.AccountBuilder;
import lesson_6.service.LoginGuiService;
import lesson_6.ui.Browser;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class SuccessLoginToYandexMailTest {

    private LoginGuiService loginGuiService = new LoginGuiService();
    private Account defaultAccount= AccountBuilder.getDefaultAccount();

    @Test(description = "Success login to yandex mail")
    public void successLoginToYandexMail() {
        loginGuiService.loginToAccountMailbox(defaultAccount);
    }

    @AfterClass
    public void stopBrowser() {
        Browser.kill();
    }

}

