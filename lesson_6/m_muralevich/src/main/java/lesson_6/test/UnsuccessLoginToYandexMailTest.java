package lesson_6.test;

import lesson_6.bo.common.AccountBuilder;
import lesson_6.bo.common.WrongAccount;
import lesson_6.service.LoginGuiService;
import lesson_6.ui.Browser;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class UnsuccessLoginToYandexMailTest {

    private LoginGuiService loginGuiService = new LoginGuiService();
    private WrongAccount wrongAccount= AccountBuilder.getWrongAccount();

    @Test(description = "Unsuccess login to yandex mail")
    public void unsuccessLoginToYandexMail() {
        loginGuiService.checkLoginImpossible(wrongAccount);
    }

    @AfterClass
    public void stopBrowser() {
        Browser.kill();
    }

}
