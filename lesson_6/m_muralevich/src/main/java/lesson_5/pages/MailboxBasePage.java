package lesson_5.pages;

import org.openqa.selenium.By;

/**
 * Created by Konstantsin_Simanenk on 3/17/2015.
 */
public class MailboxBasePage extends AbstractBasePage {

    private static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox'][@class='b-folders__folder__link']");
    private static final By SENT_LINK_LOCATOR = By.xpath("//a[@href='#sent']");
    private static final By DELETE_LINK_LOCATOR = By.xpath("//a[@href='#trash']");
    private static final By MAIL_ACCOUNT_LINK_LOCATOR = By.xpath("//span[@class='header-user-name js-header-user-name']");
    public static final By MESSAGE_WITH_ERROR_TEXT_LOCATOR = By.xpath("//div[@class='error-msg']");
    public static final By SPAM_LINK_LOCATOR = By.xpath("//a[@href='#spam']");


    public MailInboxListPage openInboxPage() {
        browser.click(INBOX_LINK_LOCATOR);
        return new MailInboxListPage();
    }

    public MailSentListPage openSentPage() {
        browser.click(SENT_LINK_LOCATOR);
        return new MailSentListPage();
    }

    public MailDeleteListPage openDeletePage() {
        browser.click(DELETE_LINK_LOCATOR);
        return new MailDeleteListPage();
    }

    public MailSpamListPage openSpamPage() {
        browser.click(SPAM_LINK_LOCATOR);
        return new MailSpamListPage();
    }

    public boolean isMailAccountLinkPresent(String mailAccount) {
        browser.waitForPresent(MAIL_ACCOUNT_LINK_LOCATOR);
        return true;
    }

    public boolean isMessageWithErrorTextPresent(String messageWithErrorText) {
        browser.waitForPresent(MESSAGE_WITH_ERROR_TEXT_LOCATOR);
        return true;
    }

}
