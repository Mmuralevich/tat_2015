package lesson_5.pages;

import org.openqa.selenium.By;

/**
 * Created by Alexander on 20.03.2015.
 */
public class MailSpamListPage extends AbstractBasePage {

    public static final By SUBJECT_SPAM_MAIL_LOCATOR = By.xpath("//div[@class='block-messages-list-box b-layout__first-pane']/div[2]//div[@class='b-messages']/div[1]//span[@class='b-messages__subject'][1]");
    public static final By SPAM_MAIL_CHECKBOX_LOCATOR = By.xpath("//div[@class='block-messages-list-box b-layout__first-pane']/div[2]//div[1]/label[@class='b-messages__message__checkbox']/input");
    public static final By NOT_SPAM_BUTTON_LOCATOR = By.xpath("//a[@data-action='notspam']");

    public boolean isMessagePresent(String subject) {
        browser.waitForVisible(SUBJECT_SPAM_MAIL_LOCATOR);
        return true;
    }

    public MailboxBasePage markEmailAsNotSpam() {
        browser.click(SPAM_MAIL_CHECKBOX_LOCATOR);
        browser.click(NOT_SPAM_BUTTON_LOCATOR);
        browser.waitForAjaxProcessed();
        return new MailboxBasePage();
    }
}
