package lesson_5.test;

import lesson_5.pages.MailInboxListPage;
import lesson_5.pages.MailSpamListPage;
import lesson_5.pages.MailboxBasePage;
import lesson_5.pages.YandexFirstPage;
import lesson_5.ui.Browser;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

/**
 * Created by Alexander on 20.03.2015.
 */
public class MarkEmailAsNotSpamTest {

    private String userLogin = "mashafakelogin";
    private String userPassword = "mashafake";
    private String mailTo = "mashafakelogin@yandex.ru";
    private String mailSubject = "test subject" + Math.random() * 100000000;
    private String mailContent = "mail content" + Math.random() * 100000000;


    @Test(description = "Mark email as spam")
    public void markEmailAsSpamTest() {
        new YandexFirstPage().open()
                .login(userLogin, userPassword)
                .openInboxPage()
                .openComposeMailPage()
                .sendMail(mailTo,mailSubject, mailContent)
                .openInboxPage()
                .markEmailAsSpam()
                .openSpamPage();

    }

    @Test(description = "Mark email as not a spam", dependsOnMethods = "markEmailAsSpamTest")
    public void markEmailAsNotSpamTest() {
        MailInboxListPage mailNotSpam = new MailSpamListPage().markEmailAsNotSpam().openInboxPage();

        Assert.assertTrue(mailNotSpam.isMessagePresent(mailSubject), "Message was not marked as not spam");

    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        Browser.get().kill();
    }
}
