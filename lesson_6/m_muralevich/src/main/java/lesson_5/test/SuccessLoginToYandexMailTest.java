package lesson_5.test;

import lesson_5.pages.YandexFirstPage;
import lesson_5.pages.MailboxBasePage;
import lesson_5.ui.Browser;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

/**
 * Created by Alexander on 19.03.2015.
 */
public class SuccessLoginToYandexMailTest {
    // Test data
    private String userLogin = "mashafakelogin";
    private String userPassword = "mashafake";
    private String mailAccountLink = "mashafakelogin@yandex.ru";

    @Test(description = "Success login to yandex mail")
    public void successLoginToYandexMail() {
        MailboxBasePage mailbox = new YandexFirstPage().open()
                .login(userLogin, userPassword);

        Assert.assertTrue(mailbox.isMailAccountLinkPresent(mailAccountLink), "User is not login to yandex mail");
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        Browser.get().kill();
    }
}

