package lesson_5.test;

import lesson_5.pages.MailboxBasePage;
import lesson_5.pages.YandexFirstPage;
import lesson_5.ui.Browser;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

/**
 * Created by Alexander on 19.03.2015.
 */
public class UnsuccessLoginToYandexMailTest {
    private String userLogin = "mashafakelogin";
    private String userPassword = "mashafake1";
    private String errorText = "Неправильная пара логин-пароль! Авторизоваться не удалось.";

    @Test(description = "Unsuccess login to yandex mail")
    public void unsuccessLoginToYandexMailTest() {
        MailboxBasePage mailbox = new YandexFirstPage().open()
                .login(userLogin, userPassword);

        Assert.assertTrue(mailbox.isMessageWithErrorTextPresent(errorText), "User is not see error message");
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        Browser.get().kill();
    }

}
