package com.epam.gomel.tat.lesson_6.test;

import com.epam.gomel.tat.lesson_6.bo.common.Account;
import com.epam.gomel.tat.lesson_6.bo.common.AccountBuilder;
import com.epam.gomel.tat.lesson_6.bo.mail.MailLetter;
import com.epam.gomel.tat.lesson_6.service.LoginGuiService;
import com.epam.gomel.tat.lesson_6.service.MailGuiService;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SendMailDemoTest {
    private LoginGuiService loginGuiService = new LoginGuiService();
    private MailGuiService mailGuiService = new MailGuiService();
    private MailLetter letter; // crate letter
    private Account defaultAccount= AccountBuilder.getDefaultAccount();

    @BeforeClass(description = "Login to account mailbox")
    public void loginToAccount() {
        loginGuiService.loginToAccountMailbox(defaultAccount);
    }

    @Test(description = "Send mail")
    public void sendMail() {
        mailGuiService.sendMail(letter);
    }

    @Test(description = "Check mail in sent list", dependsOnMethods = "sendMail")
    public void checkMailInSentList() {
        mailGuiService.checkMailInInboxList(letter);
    }

}
