package com.epam.gomel.tat;

public class HelloMain {
    public static void main(String[] args) {
        String myName = "Maryna Muralevich";
        System.out.format("Hello from %s", myName);
    }
}
